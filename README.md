# PSDK Project Backup Solution
## Requirements
You need :
- a PHP server (Apache for exemple)
- a shell interpreter (if you installed git on windows, you have it) 

## How to install
### Server side ("Server" folder)
Put index.php in a folder on your server where you can access the path of the file when browsing your server.

Since PSDK binaries can be kind of heavy (for a php server), to avoid error, you should change your php config.
You can do this by setting :
- `upload_max_filesize` to `600M`
- `post_max_size` to `601M`
- `memory_limit` to `1024M`

To do so, you can (choose one of the below methods) :
- add `init_set('<setting>','<size>');` at the start of *index.php*
- create a *.htaccess* file in the same folder as *index.php* and write `php_value <setting> <size>` (works only with Apache)
- edit the values of each setting in php.ini, format : `<setting> = <size>`

### Project side ("Utility" folder)
Put *web-backuprestore.sh* and *web-backuprestore.bat* in your PSDK project folder (or only *web-backuprestore.sh* if you're on Linux).
Then edit *web-backuprestore.sh* to make `website` match your *index.php*'s url.

**ONLY IF YOU ARE ON WINDOWS**<br/>
Set the `gitfolder` value to the path of your git installation (`"C:\Program Files\Git"` by default) or if you have another shell interpreter you'd want to use replace `%gitfolder%\bin\sh.exe` by the path to your shell interpreter.

...And it's done you'll just have to start *web-backuprestore.bat* if you're on Windows or *web-backuprestore.sh* if you're on Linux