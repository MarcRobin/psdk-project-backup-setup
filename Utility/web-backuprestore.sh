#!/bin/sh

website=http://localhost/renaissancebkup/

function backup {
   filename="$(date +%Y-%m-%d--%H-%M-%S)"_$USERNAME--"$(hostname)".tar.bz2
   echo $filename > filename.txt
   tar -jcvf $filename Audio/ Data/ Fonts/ graphics/ lib/ plugins/ ruby_builtin_dlls/ RubyHost/ Saves/ Tests/ *.url cmd.bat fmod.dll Game* Gem* Launcher* msvcrt* *.txt 
}

function installdownloaded {
   tar -jxvf update.tar.bz2
   rm update.tar.bz2
}

function update {
   backup
   curl $website --output update.tar.bz2
   installdownloaded
}

function saveonline {
   backup
   echo $filename
   curl -F 'userfile=@${filename}' "$website"index.php
}

function specificbackup {
   backup
   distfiles=$(curl $website?file=list)
   if [ "$distfiles" = "" ] 
   then
      read -p "No distant backup found! Press any key to exit..."
	  exit
   fi
   select choicefile in $distfiles; do
      curl $website?file=$choicefile --output update.tar.bz2
	  break
   done
   installdownloaded
}

echo "What do you want to do with current project (Event-wise and ressource-wise only)?"
select choice in "Backup Only" "Backup & Update" "Backup & Save Online" "Restore specific online backup" "Quit"; do
   case $choice in
      "Backup & Update") update; break;;
      "Backup Only") backup; break;;
      "Backup & Save Online") saveonline; break;;
      "Restore specific online backup") specificbackup; break;;
	  "Quit") exit;;
   esac
done
