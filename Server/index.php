<?php

$uploaddir = 'uploads/';

if ( !is_dir( $uploaddir ) ) {
    mkdir( $uploaddir );       
}

if (empty($_GET)) {
	if (empty($_FILES)) {
		var_dump($_FILES);
		readfile($uploaddir . scandir($uploaddir, 1)[0]);
	} else {
		$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
		
		header('Content-Type: text/html; charset=utf-8');
		echo '<pre>';
		if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
			echo "File successfuly uploaded. More infos :\n";
		} else {
			echo "What do you think your trying to do? >:/ \n";
		}

		echo 'Here are some debug infos :';
		print_r($_FILES);

		echo '</pre>';
	}
} elseif (isset($_GET["file"])) {
	if ($_GET["file"] == "list") {
		foreach (scandir($uploaddir, 1) as $k => $v) {
			if ($v != ".." and $v != ".") {
				echo "$v ";
			}
		}
	} elseif (in_array($_GET["file"], scandir($uploaddir))) {
		readfile($uploaddir . $_GET["file"]);
	} else {
		echo "This file doesn't exist";
	}
} else {
	echo "Wrong parameter";
}
?>
